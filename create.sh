#!/bin/bash

DBPEDIA_DIR=/nfsmnt/proto_space/dbpedia
PYSPARK=/usr/local/spark-1.5.1-bin-hadoop2.4/bin/spark-submit
RDF2HDT=$DBPEDIA_DIR/hdt-cpp/hdt-lib/tools/rdf2hdt
SERDI=serdi
DBPEDIA_VERSION=3.8
HDT_DIR=$DBPEDIA_DIR/hdt_files
# Optional
OWL_TO_NT_CONVERTER=$DBPEDIA_DIR/rdf2rdf-1.0.1-2.3.1.jar


echo "This process will clean and verify the dbpedia triples file,
and then create the HDT file.

---------------------------------------------------------------------

DBPEDIA_DIR: $DBPEDIA_DIR
PYSPARK: $PYSPARK
RDF2HDT: $RDF2HDT
SERDI: $SERDI
DBPEDIA_VERSION: $DBPEDIA_VERSION

IMPORTANT:
The Ontology file to be included with the HDT is expected 
to be named: dbpedia_ontology.nt
This script will try to convert the ontology file from 
dbpedia_ontology.owl to .nt if the OWL_TO_NT_CONVERTER is specified.

The triples file is expected to be named: dbpedia
The output of the spark process is stored in a directory named "valid"
and will be cleaned up after this program is completed.

NOTE ::: Ensure atleast three times as much free space in the target disk
as the size of the dbpedia triples file.

This is going to take a while..."

cd $DBPEDIA_DIR/$DBPEDIA_VERSION

if [ ! -f "dbpedia" ]; then
    echo "Triples file 'dbpedia' not found. Attempting to decompress the bz2 files to create it."
    bzip2 -dkc *.bz2 > dbpedia
    echo "The file 'dbpedia' created."
fi

echo "Starting to clean the triples for version $DBPEDIA_VERSION"

$PYSPARK $DBPEDIA_DIR/dbp_clean.py
if [ $? -eq 0 ]; then
    echo "Cleaning completed successfully."
else
    echo "pyspark failed! Exiting..."
    exit $E_OPTERROR
fi

echo "Creating the clean triples file."

cat valid/part-* > all.nt

if [ $? -ne 0 ]; then
    echo "Error creating the triples file all.nt"
    exit $E_OPTERROR
fi

echo "Done. Removing the temporary output directory."
rm -rf valid

if [ -f "dbpedia_ontology.owl" ]; then
    echo "dbpedia_ontology.owl file found. Converting it to .nt"
    if [ ! -f $OWL_TO_NT_CONVERTER ]; then
        echo "No RDF/OWL to NT converter found in env OWL_TO_NT_CONVERTER"
        exit $E_OPTERROR
    fi
    java -jar $OWL_TO_NT_CONVERTER dbpedia_ontology.owl dbpedia_ontology.nt
    if [ $? -eq 0 ]; then
        echo "successfully converted .owl to .nt"
    else
        echo "Conversion to .nt failed. Exiting...."
        exit $E_OPTERROR
    fi
fi

if [ -f "dbpedia_ontology.nt" ]; then
    echo "Appending the ontology to the triples."
    cat dbpedia_ontology.nt >> all.nt
fi

echo "Verifying syntax with serdi."
serd_em=`$SERDI -i ntriples all.nt > /dev/null 2>err.log`
if [ $? -ne 0 ]; then
    echo "Serdi failed. Syntax error in the the triples file."
    echo $serd_em
    exit
fi

echo "Syntax check completed."

echo "Creating HDT"

hdt_em=`$RDF2HDT -f turtle all.nt $HDT_DIR/$DBPEDIA_VERSION.hdt`
if [ $? -ne 0 ]; then
    echo "HDT creation failed!"
    echo $hdt_em
    exit
fi

echo "HDT, $HDT_DIR/$DBPEDIA_VERSION.hdt created successfully."

