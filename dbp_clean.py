### #!/usr/local/spark-1.5.1-bin-hadoop2.4/bin/spark-submit

import re
import urllib
import urlparse
import subprocess
from pyspark import SparkContext, SparkConf
import RDF



# Django's regex url validation code
url_val = re.compile(
        r'^(?:http)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

url_validator = re.compile(r'.*(\{|\}|"|`|\\|\^|\||\s+).*')

def cleanup(line):
    line = line.encode("utf8")
    url_start = [i for i, l in enumerate(line) if l == "<"]
    url_end = [i for i, l in enumerate(line) if l == ">"]

    if not url_start or not url_end or len(url_start) != len(url_end):
        return ""

    if url_start[0] != 0:
        return ""

    clean_line = line[:url_start[0]+1]
    for i, us in enumerate(url_start):
        url = line[us+1:url_end[i]]
        if not url or url == "" or not url_val.match(url):
            return ""

        url_parts = urlparse.urlparse(url)

        up = url_parts.path + url_parts.query
        if url_parts.path or url_parts.query:
            up = url.split(url_parts.netloc)[1]

        if url_validator.match(up):
            #up = up.encode("string-escape")
            ## TODO: find a better way to handle bad encoding... 
            up = up.encode("string-escape").replace("\\", "")
            clean_line += url_parts.scheme + "://" + url_parts.netloc + re.sub('\{|\}|"|`|\^|\||\s+', "", up)
        else:
            clean_line += url

        if i+1 < len(url_start):
            clean_line += line[url_end[i]:url_start[i+1]+1]

    clean_line += line[url_end[-1]:]
    return clean_line


def rdf_check(line):

    parser = RDF.NTriplesParser()
    base_uri = "http://dbpedia.org"
    try:
        parser.parse_string_as_stream(line, base_uri)
    except:
        return ""
    return cleanup(line)


conf = (
        SparkConf()
        .setMaster("local[*]")
        .setAppName("DBpedia")
        .set("spark.executor.memory", "1g")
        .set("spark.deploy.defaultCores", "64")
        .set("spark.cores.max", "64")
        )

sc = SparkContext(conf=conf)

triples = sc.textFile("dbpedia")
#clean_triples = triples.map(cleanup)
clean_triples = triples.map(rdf_check)
clean_triples.saveAsTextFile("valid")
