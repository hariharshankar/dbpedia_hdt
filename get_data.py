import commands
from threading import Thread, active_count
import time

NUM_THREADS = 5
SLEEPTIME=5
DBP_URL = "http://downloads.dbpedia.org/2015-10/core-i18n/en/%s"
DEST = "201510"
CURL_CMD = "curl -s -L --connect-timeout 30 \"%s\" > %s/%s"


class FetchThread(Thread):
	def run(self):
		commands.getoutput(CURL_CMD % (self.url, DEST, self.filename))
		print("completed: " + self.url)


with open(DEST + "/links.txt") as links:
	for link in links:
		link = link.split()[0]
		if link.find("_en.ttl") > 0:
			url = DBP_URL % link
			print("started: " + url)
			while active_count() >= NUM_THREADS:
				time.sleep(SLEEPTIME)

			t = FetchThread()
			t.url = url
			t.filename = link.split("/")[-1]
			t.start()

