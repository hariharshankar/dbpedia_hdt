# DBpedia Cleanup Scripts for HDT Creation

All versions of the [DBpedia Data](http://downloads.dbpedia.org/) were downloaded in n-triples format.

## Softwares and Dependencies

* [Apache Spark](https://spark.apache.org/)  -- The code tested with version 1.5.1.
* [libcds](https://github.com/LinkedDataFragments/hdt-cpp/tree/master/libcds-v1.0.12) -- Required for HDT C++.
* [Raptor](http://librdf.org/raptor/)  -- Required for HDT-C++.
* [Kyoto Cabinet](http://fallabs.com/kyotocabinet/) -- Required for HDT-C++.
* [HDT-C++](https://github.com/LinkedDataFragments/hdt-cpp)
Edit Makefile uncommenting these lines to include compile support for the optional libraries:
RAPTOR_SUPPORT=true
KYOTO_SUPPORT=true
LIBZ_SUPPORT=true
* [Serd](http://drobilla.net/software/serd/)  -- Checks n-triples file for syntax/URI errors.
* [Rasqal](http://librdf.org/rasqal/INSTALL.html)  -- Required for Redland
* [Redland RDF Libraries](http://librdf.org/) -- A library that provides n-triples syntax checks similar to Serd.
* [Redland Language Bindings for Python](http://librdf.org/docs/python.html) -- Provides Python bindings for the syntax checker.
Download the bindings from: http://librdf.org/bindings/

## Run

After downloading the DBpedia data, executing `./create.sh` will process the data, and create the hdt. 

__NOTE: The `create.sh` file will have to be edited according to your system.__

## Workflow

### Download the DBpedia version. 

The [get_data.py](https://bitbucket.org/hariharshankar/dbpedia_hdt/src/3c55c7b23ee5dd9bc7d8e11cd7344fd2a19dd3e7/get_data.py?fileviewer=file-view-default) script can be used. This script expects the file names to be download in a file called links.txt in the target directory, with one file per line. The script may need to be modified for each version depending on the file extension expected.

### Prepare the version for processing

Decompress the bz2 files and create one file containing all the triples:
```
#!bash
bzip2 -dk *.bz2 > dbpedia
```

### Clean the triples

The [dbp_clean.py](https://bitbucket.org/hariharshankar/dbpedia_hdt/src/3c55c7b23ee5dd9bc7d8e11cd7344fd2a19dd3e7/dbp_clean.py?at=master&fileviewer=file-view-default) script uses Spark and the Redland RDF library to check and clean the the triples. If a line of triple fails the syntax check, it is removed. The results are saved in a folder called `valid` in the target directory. 

```
#!bash

# change the pyspark path as necessary.
$ ./dbp_clean.py 
$ cat valid/part-* > all.nt

```
To make sure the new triples are absolutely clean, check them with Serd.
```
#!bash
$ serdi -i ntriples all.nt > serd.nt
```
Serd should exit without errors if all is okay. If there are errors, check brute force cleaning below.


### Process the HDT
Use the clean triple file to create the HDT.
```
#!bash
hdt-cpp/hdt-lib/tools/rdf2hdt -f turtle all.nt output.hdt
```

### Brute Force cleaning and creating HDT:
If there are errors reported by Serd even after using the `dbp_clean.py` script, follow the steps below.

* check the different files with SERD parser:

  `serdi -i ntriples xxxx.nt | wc -l`

* in case errors occur, repair/delete matching lines with, e.g., grep -v
* concatenate everything and check with SERD to be sure.

  `serdi -i ntriples *.nt > all.nt`

* generate HDT with turtle parser

  `rdf2hdt -f turtle all.nt all.hdt`

* test HDT testen by generating index file  .hdt.index

  `hdtSearch -q 0 all.hdt`